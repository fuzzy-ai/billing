// api-plans-test.coffee
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const PaymentsServerMock = require('./payments-mock')
const StripeServerMock = require('./stripe-mock')

const env = require('./config')

process.on('uncaughtException', err => console.error(err))

vows
  .describe('GET /')
  .addBatch({
    'When we start a mock Stripe server': {
      topic () {
        const { callback } = this
        const stripe = new StripeServerMock()
        stripe.start(err => callback(err, stripe))
        return undefined
      },
      'it works' (err, stripe) {
        assert.ifError(err)
        assert.isObject(stripe)
      },
      'teardown' (stripe) {
        stripe.stop(this.callback)
      },
      'When we start a mock Payments server': {
        topic () {
          const { callback } = this
          const payments = new PaymentsServerMock()
          payments.start(err => callback(err, payments))
          return undefined
        },
        'it works' (err, payments) {
          assert.ifError(err)
          assert.isObject(payments)
        },
        'teardown' (payments) {
          payments.stop(this.callback)
        },
        'and we start a BillingServer': {
          topic (payments, stripe) {
            const { callback } = this
            try {
              const BillingServer = require('../lib/billingserver')
              const server = new BillingServer(env)
              server.express.stripe = require('stripe')('test_00000000000')
              server.express.stripe.setHost('localhost', 1516, 'http')
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            server.stop(this.callback)
          },
          'and we GET the root document': {
            topic (server, payments, stripe) {
              const options =
                {url: 'http://localhost:2342/'}
              request.get(options, this.callback)
              return undefined
            },
            'it works' (err, response, body) {
              assert.ifError(err)
              assert.isObject(response)
              assert.equal(response.statusCode, 200)
              assert.isString(body)
            },
            'it returns an HTML document' (err, response, body) {
              assert.ifError(err)
              const expected = 'text/html'
              const ct = response.headers['content-type']
              const ctbase = ct != null ? ct.substr(0, expected.length) : undefined
              assert.equal(ctbase, expected)
            }
          }
        }
      }
    }}).export(module)
