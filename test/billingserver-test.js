// billingserver-test.coffee
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./config')

vows
  .describe('Billing server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const BillingServer = require('../lib/billingserver')
          callback(null, BillingServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, BillingServer) {
        assert.ifError(err)
      },
      'it is a class' (err, BillingServer) {
        assert.ifError(err)
        assert.isFunction(BillingServer)
      },
      'and we instantiate an BillingServer': {
        topic (BillingServer) {
          const { callback } = this
          try {
            const server = new BillingServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            server.start((err) => {
              if (err) {
                callback(err)
              } else {
                callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  body = JSON.parse(body)
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, version) {
              assert.ifError(err)
            },
            'it looks correct' (err, version) {
              assert.ifError(err)
              assert.include(version, 'version')
              assert.include(version, 'name')
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    callback(err)
                  } else {
                    callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      callback(null)
                    } else {
                      callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
