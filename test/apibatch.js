// api-batch.coffee
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows

const _ = require('lodash')

const PaymentsServerMock = require('./payments-mock')
const StripeServerMock = require('./stripe-mock')

const env = require('./config')

const base = {
  'When we start a mock Stripe server': {
    topic () {
      const { callback } = this
      const stripe = new StripeServerMock()
      stripe.start(err => callback(err, stripe))
      return undefined
    },
    'it works' (err, stripe) {
      assert.ifError(err)
      assert.isObject(stripe)
    },
    'teardown' (stripe) {
      stripe.stop(this.callback)
    },
    'When we start a mock Payments server': {
      topic () {
        const { callback } = this
        const payments = new PaymentsServerMock('unittestkey')
        payments.start(err => callback(err, payments))
        return undefined
      },
      'it works' (err, payments) {
        assert.ifError(err)
        assert.isObject(payments)
      },
      'teardown' (payments) {
        payments.stop(this.callback)
      },
      'and we start a BillingServer': {
        topic (payments, stripe) {
          const { callback } = this
          try {
            const BillingServer = require('../lib/billingserver')
            const server = new BillingServer(env)
            server.express.stripe = require('stripe')('test_00000000000')
            server.express.stripe.setHost('localhost', 1516, 'http')
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        }
      }
    }
  }
}

const apiBatch = function (rest) {
  const batch = _.cloneDeep(base)
  _.extend(batch['When we start a mock Stripe server']['When we start a mock Payments server']['and we start a BillingServer'], rest)
  return batch
}

module.exports = apiBatch
