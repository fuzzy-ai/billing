// event-test.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('Event data type')
  .addBatch({
    'When we load the Event module': {
      topic () {
        try {
          const Event = require('../lib/event')
          this.callback(null, Event)
        } catch (err) {
          this.callback(err, null)
        }
        return undefined
      },
      'it works' (err, Event) {
        assert.ifError(err)
        assert.isFunction(Event)
      },
      'and we set up the database': {
        topic (Event) {
          const { callback } = this
          const params = JSON.parse(env.PARAMS)
          params.schema = {event: Event.schema}
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              callback(err, null)
            } else {
              DatabankObject.bank = db
              callback(null, db)
            }
          })
          return undefined
        },
        'it works' (err, db) {
          assert.ifError(err)
          assert.isObject(db)
        },
        'teardown' (db) {
          const { callback } = this
          if (db) {
            db.disconnect(callback)
          } else {
            callback(null)
          }
          return undefined
        },
        'and we create a new Event': {
          topic (db, Event) {
            const { callback } = this
            const props = {
              id: 'evt_17YIO82eZvKYlo2CoLE0ksi5',
              created: Math.round((new Date()).getTime() / 1000),
              data: {},
              request: null,
              type: 'charge.succeeded'
            }
            Event.create(props, (err, event) => {
              if (err) {
                callback(err)
              } else {
                callback(null, event)
              }
            })
            return undefined
          },
          'it works' (err, event) {
            assert.ifError(err)
            assert.isObject(event)
            assert.isString(event.id)
            assert.isString(event.createdAt)
            assert.inDelta(Date.parse(event.createdAt), Date.now(), 5000)
            assert.isObject(event.data)
            assert.isString(event.type)
          }
        }
      }
    }}).export(module)
