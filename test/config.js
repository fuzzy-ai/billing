// config.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const env = {
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  LOG_FILE: '/dev/null',
  APP_KEY_UNIT_TEST: 'verdipuckrinkbanwilt',
  BILLING_PAYMENTS_SERVER: 'http://localhost:1517',
  BILLING_PAYMENTS_KEY: 'unittestkey'
}

module.exports = env
