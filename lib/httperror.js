// httperror.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

class HTTPError extends Error {
  constructor (message, statusCode) {
    super(message)
    this.statusCode = statusCode
  }
}

module.exports = HTTPError
