// event.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const db = require('databank')

const Event = db.DatabankObject.subClass('event')

Event.schema = {
  pkey: 'id',
  fields: [
    'type',
    'request',
    'data',
    'createdAt'
  ],
  indices: ['type']
}

Event.beforeCreate = function (props, callback) {
  const required = ['id', 'type', 'data', 'created']
  for (const prop of Array.from(required)) {
    if (!props[prop]) {
      callback(new Error(`${prop} property required`))
    }
  }

  props.createdAt = (new Date(props.created * 1000)).toISOString()
  callback(null, props)
}

module.exports = Event
