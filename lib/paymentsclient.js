// paymentsclient.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const MicroserviceClient = require('@fuzzy-ai/microservice-client')

class PaymentsClient extends MicroserviceClient {
  addCustomer (data, callback) {
    return this.post('/customers', data, callback)
  }

  updateCustomer (customerId, data, callback) {
    return this.put(`/customers/${customerId}`, data, callback)
  }

  customerRefresh (customerId, data, callback) {
    return this.post(`/customers/${customerId}/refresh`, data, callback)
  }

  addInvoice (data, callback) {
    return this.post('/invoices', data, callback)
  }

  updateInvoice (invoiceId, data, callback) {
    return this.put(`/invoices/${invoiceId}`, data, callback)
  }

  invoicePaid (data, callback) {
    return this.post('/invoices/paid', data, callback)
  }

  version (callback) {
    return this.get('/version', callback)
  }
}

module.exports = PaymentsClient
