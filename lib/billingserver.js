// billingserver.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.io>
// All rights reserved.

const debug = require('debug')('billing:billingserver')
const async = require('async')

const Microservice = require('@fuzzy-ai/microservice')

const version = require('./version')
const Event = require('./event')
const HTTPError = require('./httperror')

const PaymentsClient = require('./paymentsclient')

class BillingServer extends Microservice {
  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)
    cfg.stripeKey = env['STRIPE_KEY']
    cfg.paymentsServer = env['BILLING_PAYMENTS_SERVER']
    cfg.paymentsKey = env['BILLING_PAYMENTS_KEY']
    cfg.homeURL = env['HOME_URL'] || 'https://fuzzy.ai/'
    cfg.redirectToHttps = this.envBool(env, 'REDIRECT_TO_HTTPS', false)

    if (cfg.stripeKey) {
      this.stripe = require('stripe')(cfg.stripeKey)
    }

    return cfg
  }

  setupExpress () {
    const exp = super.setupExpress()
    exp.stripe = this.stripe
    exp.payments = new PaymentsClient(this.config.paymentsServer,
      this.config.paymentsKey)
    return exp
  }

  setupMiddleware (exp) {
    // Redirect if we're HTTPS and getting hit by HTTP

    if (this.config.redirectToHttps) {
      exp.use((req, res, next) => {
        if (req.headers['x-forwarded-proto'] === 'http') {
          return res.redirect(301, `https://${req.hostname}${req.url}`)
        } else {
          return next()
        }
      })
    }

    return exp
  }

  setupRoutes (exp) {
    exp.get('/', this._getRoot.bind(this))

    exp.post('/stripe/webhook', this._postWebhook.bind(this))

    exp.get('/live', this.dontLog.bind(this), this._getLive.bind(this))
    exp.get('/ready', this.dontLog.bind(this), this._getReady.bind(this))

    return exp.get('/version', (req, res, next) => res.json({name: 'billing', version}))
  }

  _getRoot (req, res, next) {
    res.set('Content-Type', 'text/html')
    return res.send(`\
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="0;URL='${this.config.homeURL}'" />
    <title>Nothing to see here</title>
  </head>
  <body>
    <h1>Nothing to see here</h1>
    <p>
      There's nothing interesting here.
      Try <a href='${this.config.homeURL}'>${this.config.homeURL}</a>.
    </p>
  </body>
</html>\
`
    )
  }

  _postWebhook (req, res, next) {
    const { stripe } = req.app
    const { payments } = req.app
    const eventJSON = req.body

    const handleResponse = function (err, response) {
      if (err) {
        return next(err)
      } else {
        return Event.create(eventJSON, (err, event) => {
          if (err) {
            return next(err)
          } else {
            return res.sendStatus(200)
          }
        })
      }
    }

    if (eventJSON) {
      // Verify the event
      return stripe.events.retrieve(eventJSON.id, (err, event) => {
        if (err) {
          return next(err)
        } else {
          // Make sure we haven't seen this event
          return Event.get(event.id, (err, result) => {
            if ((err != null ? err.name : undefined) === 'NoSuchThingError') {
              switch (event.type) {
                case 'invoice.created':
                  let invoice = event.data.object
                  return payments.addInvoice(invoice, handleResponse)
                case 'invoice.updated':
                  invoice = event.data.object
                  return payments.updateInvoice(invoice.id, invoice, handleResponse)
                case 'invoice.payment_succeeded':
                  invoice = event.data.object
                  return payments.invoicePaid(invoice, handleResponse)
                case 'customer.subscription.created' ||
                       'customer.subscription.updated':
                  const subscription = event.data.object
                  const id = subscription.customer
                  return payments.customerRefresh(id, subscription, handleResponse)
                default:
                  return res.sendStatus(200)
              }
            } else if (err) {
              return next(err)
            } else {
              return res.sendStatus(200)
            }
          })
        }
      })
    } else {
      return res.status(500)
    }
  }

  _getLive (req, res, next) {
    return res.json({status: 'OK'})
  }

  _getReady (req, res, next) {
    if ((this.db == null)) {
      return next(HTTPError('Database not connected', 500))
    }

    return async.parallel([
      callback => {
        // Check that we can contact the database
        return this.db.save('server-ready', 'billing', 1, callback)
      },
      callback => req.app.payments.version(callback),
      callback =>
        // Check that we can reach Stripe
        // FIXME: this seems like a heavyweight API call to make; maybe
        // there's something slimmer?
        req.app.stripe.balance.retrieve(callback)

    ], (err) => {
      if (err != null) {
        debug(`${err.name}: ${err.message} in /ready`)
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }

  getSchema () {
    return {
      'event': Event.schema
    }
  }
}

module.exports = BillingServer
