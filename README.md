# billing
Billing endpoint for fuzzy.io for Stripe webhooks

## Environment Variables

* `STRIPE_KEY`: The Stripe secret key to use (for the Stripe API).
* `HOME_URL`: Home page for our site; we redirect on a hit to the root.
  Default is "https://fuzzy.ai/" but you may want to change it in a dev env
  or another reason...?
* `REDIRECT_TO_HTTPS`: boolean; redirect to https:// version of http:// URLs.
  
## Endpoints

* `POST /stripe/webhook`: [Stripe Webhook](https://stripe.com/docs/webhooks) endpoint for stripe to receive events from Stripe.
