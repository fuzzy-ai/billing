FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/billing
COPY . /opt/billing

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/billing/bin/dumb-init", "--"]
CMD ["npm", "start"]
